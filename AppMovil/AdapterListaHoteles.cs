﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;

namespace AppMovil
{
    class AdapterListaHoteles : BaseAdapter<Hotel>
    {

        List<Hotel> listaHotel;
        Activity context;

        public AdapterListaHoteles(Context context, List<Hotel> itemsHotel) : base()
        {
            this.context = (Activity)context;
            this.listaHotel = itemsHotel;
        }


        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = listaHotel[position];
            var view = convertView;
            view = context.LayoutInflater.Inflate(Resource.Layout.DataRow, null);
            string archivo = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), item.rutaFoto);
            view.FindViewById<TextView>(Resource.Id.txtNombreHotel).Text = item.nombre;
            var txtLugar = view.FindViewById<TextView>(Resource.Id.txtLugar);
            txtLugar.Text = item.direccion;
            var bitmap = BitmapFactory.DecodeFile(archivo);
            var imgFoto = view.FindViewById<ImageView>(Resource.Id.imagenFondo);
            imgFoto.SetImageBitmap(bitmap);
            return view;
        }

        //Fill in cound here, currently 0
        public override int Count
        {
            get
            { return listaHotel.Count; }
        }
        public override Hotel this[int position]
        {
            get
            { return listaHotel[position]; }
        }

    }

    class AdapterListaHotelesViewHolder : Java.Lang.Object
    {
        //Your adapter views to re-use
        //public TextView Title { get; set; }
    }
}