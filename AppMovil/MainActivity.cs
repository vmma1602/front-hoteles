﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Widget;


namespace AppMovil
{
    [Activity(Label = "HostPro", Theme = "@style/Theme.AppCompat.Light.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            var btnBuscarHotel = FindViewById<Button>(Resource.Id.btnBuscarHoteles);
            var btnPublcar = FindViewById<Button>(Resource.Id.btnHotel);


            btnBuscarHotel.Click += delegate
            {
                AbrirPrincipal();
            };
            btnPublcar.Click += delegate
            {
                AbrirInicio();
            };

        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public void AbrirPrincipal()
        {

            Intent destino = new Intent(this, typeof(Principal));
            StartActivity(destino);
        }


        public void AbrirInicio()
        {
            Intent destino = new Intent(this, typeof(InicioSesion));
            StartActivity(destino);
        }
    }

    public class Hotel
    {
        public int idHotel { get; set; }
        public string nombre { get; set; }
        public string correoHotel { get; set; }
        public string pass { get; set; }
        public string descripcion { get; set; }
        public string direccion { get; set; }
        public double latitud { get; set; }
        public double longitud { get; set; }
        public double precioHospedaje { get; set; }

        public string rutaFoto { get; set; }

    }
}