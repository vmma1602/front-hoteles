﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace AppMovil
{
    [Activity(Label = "Registro")]
    public class Registro : Activity
    {
        EditText txtNombre, txtCorreo, txtPass, txtConfirmarPass;
        CheckBox checar;
        Button btnGuardar, btnRegresar;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.registro);
            txtNombre = FindViewById<EditText>(Resource.Id.txtNombreUsuario);
            txtCorreo = FindViewById<EditText>(Resource.Id.txtCorreoUsuario);
            txtPass = FindViewById<EditText>(Resource.Id.txtContraseñaUsiario);
            txtConfirmarPass = FindViewById<EditText>(Resource.Id.txtVerificarContraseña);
            btnGuardar = FindViewById<Button>(Resource.Id.btnGuardar);
            btnRegresar = FindViewById<Button>(Resource.Id.btnRegresar);
            checar = FindViewById<CheckBox>(Resource.Id.checkBox);


            btnRegresar.Click += delegate
            {
                this.Finish();
            };

            btnGuardar.Click += delegate
            {
                PasarDatosRegistroUsuario();
            };




        }

        public void PasarDatosRegistroUsuario()
        {
            if (txtCorreo.Text != "" && txtNombre.Text != "" && txtPass.Text != "")
            {
                if (txtPass.Text == txtConfirmarPass.Text)
                {
                    if (checar.Checked)
                    {
                        Intent destino = new Intent(this, typeof(MenuHotelero));
                        destino.PutExtra("nombreUsuario", txtNombre.Text);
                        destino.PutExtra("correoUsuario", txtCorreo.Text);
                        destino.PutExtra("passUsuario", txtPass.Text);
                        StartActivity(destino);
                        Finish();
                    }
                    else
                    {
                        Toast.MakeText(this, "No está aceptando los términos", ToastLength.Short).Show();
                    }
                }
                else
                {
                    Toast.MakeText(this, "Las contraseñas no coinciden", ToastLength.Short).Show();
                    txtPass.Text = "";
                    txtConfirmarPass.Text = "";
                }
            }
            else
            {
                Toast.MakeText(this, "Porfavor, llene los campos", ToastLength.Short).Show();

            }

        }

    }
}