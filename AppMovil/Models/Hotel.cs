﻿namespace API_Hoteles.Models
{
    public class Hotel
    {
        public int idHotel { get; set; }
        public string nombre { get; set; }
        public string correoHotel { get; set; }
        public string pass { get; set; }
        public string descripcion { get; set; }
        public string direccion { get; set; }
        public double latitud { get; set; }
        public double longitud { get; set; }
        public double precioHospedaje { get; set; }
        public string rutaFoto { get; set; }


    }
}
