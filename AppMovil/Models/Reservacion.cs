﻿using System;

namespace API_Hoteles.Models
{
    public class Reservacion
    {
        public int idReservacion { get; set; }
        public int numPersonas { get; set; }
        public DateTime fechaLlegada { get; set; }
        public DateTime fechaSalida { get; set; }
        public int fkHotel { get; set; }
    }
}
