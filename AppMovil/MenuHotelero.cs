﻿using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
namespace AppMovil
{
    [Activity(Label = "MenuHotelero")]
    public class MenuHotelero : Activity
    {
        ListView listviewHoteles;
        List<Hotel> listaHotel = new List<Hotel>();
        string correo, pass;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.MenuHotelero);
            listviewHoteles = FindViewById<ListView>(Resource.Id.listaHoteles);
            var btnRegistroNuevo = FindViewById<Button>(Resource.Id.btnRegistrarHotelNuevo);
            UserDialogs.Init(this);
            var progresos = UserDialogs.Instance.Loading("Cargando hoteles", null, null, true, MaskType.Clear);
            await CargarHoteles();
            progresos.Hide();
            var btnRegresar = FindViewById<Button>(Resource.Id.btnRegresar);
            pass = Intent.GetStringExtra("passUsuario");
            correo = Intent.GetStringExtra("correoUsuario");
            btnRegresar.Click += delegate
            {
                this.Finish();
            };


            btnRegistroNuevo.Click += delegate
            {
                AbrirRegistroNuevoHotel();
            };
        }

        public async Task CargarHoteles()
        {
            try
            {
                string Datosjson = await TraerDatos("https://apihoteles.azurewebsites.net/api/Hoteles/listahotelesid?correo=" + Intent.GetStringExtra("correoUsuario") + "");
                var numeroRegistros = JArray.Parse(Datosjson);


                string archivo = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                for (int i = 0; i < numeroRegistros.Count; i++)
                {
                    JObject data = JObject.Parse(numeroRegistros[i].ToString());
                    Hotel h = new Hotel();
                    h = JsonConvert.DeserializeObject<Hotel>(data.ToString());
                    await bajarBlob(h.rutaFoto);
                    listaHotel.Add(h);
                }
                listviewHoteles.Adapter = new AdapterListaHoteles(this, listaHotel);
                listviewHoteles.ItemClick += OnListItemClick;
            }
            catch
            {

            }
        }


        public async Task<bool> bajarBlob(string nombre)
        {
            try
            {
                string archivo = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), nombre);
                var cuentaBlob = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=programacionmoviles2021;AccountKey=/MEr4ZzBjKxKAbaTKUvM0vfG14AZf2XqlLe4AQC3rE2D2q4C3oYqYeThrhNPDJCp4WaKVVxGZiDz0uCVAOgFPA==;EndpointSuffix=core.windows.net");
                var clienteBlob = cuentaBlob.CreateCloudBlobClient();
                var Carpeta = clienteBlob.GetContainerReference("munoz-victor");
                var resourceBlob = Carpeta.GetBlockBlobReference(nombre);

                var fs = new FileStream(archivo, FileMode.OpenOrCreate);
                await resourceBlob.DownloadToStreamAsync(fs);




                return true;
            }
            catch (System.Exception ex)
            {
                return false;

            }
        }
        public void OnListItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var DataSend = listaHotel[e.Position];
            var DataIntent = new Intent(this, typeof(VerHotelesRegistrados));
            DataIntent.PutExtra("nombre", DataSend.nombre);
            DataIntent.PutExtra("pass", DataSend.pass);
            DataIntent.PutExtra("idHotel", DataSend.idHotel.ToString());
            DataIntent.PutExtra("direccion", DataSend.direccion);
            DataIntent.PutExtra("descripcion", DataSend.descripcion);
            DataIntent.PutExtra("correoHotel", DataSend.correoHotel);
            DataIntent.PutExtra("precioHospedaje", DataSend.precioHospedaje.ToString());
            DataIntent.PutExtra("rutaFoto", DataSend.rutaFoto);
            DataIntent.PutExtra("lat", DataSend.latitud.ToString());
            DataIntent.PutExtra("lon", DataSend.longitud.ToString());
            //Ocupo un capmo tipo int de calificacion en la tabla jeje
            StartActivity(DataIntent);
            Finish();


        }
        public async Task<string> TraerDatos(string API)
        {

            var request = (HttpWebRequest)WebRequest.Create(API);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return await reader.ReadToEndAsync();

        }
        public void AbrirRegistroNuevoHotel()
        {
            Intent destino = new Intent(this, typeof(RegistroDeHotel));
            destino.PutExtra("correoUsuario", correo);
            destino.PutExtra("passUsuario", pass);
            StartActivity(destino);

            Finish();
        }
    }
}