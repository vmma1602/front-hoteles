﻿
using Android.App;
using Android.OS;

namespace AppMovil
{
    [Activity(Label = "ElegirUbicacion")]
    public class ElegirUbicacion : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.elegir_ubicacion);
        }
    }
}