﻿using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using Microsoft.WindowsAzure.Storage;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace AppMovil
{
    [Activity(Label = "VerHotelesRegistrados")]
    public class VerHotelesRegistrados : Activity, IOnMapReadyCallback
    {
        TextView txtNombre, txtDescripcion, txtCorreo, txtDireccion, txtCosto, txtServicio1, txtServicio2, txtServicio3;
        RatingBar ratingBar;
        ImageView Imagen;
        GoogleMap googleMap;
        string correo, imagen, nombre, descripcion, direccion, imagenfondo, pass, servicio2, servicio3;
        double costo, lat, lon;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.ver_hoteles_registrados);
            var btnEliminar = FindViewById<Button>(Resource.Id.btnEliminar);
            var btnGuardar = FindViewById<Button>(Resource.Id.btnGuardar);
            var btnRegresar = FindViewById<Button>(Resource.Id.btnRegresar);
            var idHotel = Intent.GetStringExtra("idHotel");
            nombre = Intent.GetStringExtra("nombre");
            descripcion = Intent.GetStringExtra("descripcion");
            direccion = Intent.GetStringExtra("direccion");
            correo = Intent.GetStringExtra("correoHotel");
            costo = double.Parse(Intent.GetStringExtra("precioHospedaje"));
            lat = double.Parse(Intent.GetStringExtra("lat"));
            lon = double.Parse(Intent.GetStringExtra("lon"));
            imagen = Intent.GetStringExtra("rutaFoto");
            pass = Intent.GetStringExtra("pass");
            //ratingBar = (Intent.GetStringExtra("calificacion");

            txtNombre = FindViewById<TextView>(Resource.Id.txtNombreHotel);
            txtDescripcion = FindViewById<TextView>(Resource.Id.txtDescripcion);
            txtCorreo = FindViewById<TextView>(Resource.Id.txtCorreo);
            txtDireccion = FindViewById<TextView>(Resource.Id.txtDireccion);
            txtCosto = FindViewById<TextView>(Resource.Id.txtCosto);
            Imagen = FindViewById<ImageView>(Resource.Id.imagenFondo);
            RatingBar ratingBar = FindViewById<RatingBar>(Resource.Id.ratingBar);
            ratingBar.Rating = 4;



            txtNombre.Text = nombre;
            txtDescripcion.Text = descripcion;
            //txtDireccion.Text = direccion;
            txtCorreo.Text = correo.ToString();
            txtCosto.Text = costo.ToString();
            var rutaimagen = System.IO.Path.Combine(System.Environment.GetFolderPath
                (System.Environment.SpecialFolder.Personal), imagen);

            Bitmap bit = BitmapFactory.DecodeFile(rutaimagen);
            Imagen.SetImageBitmap(bit);

            var mapView = FindViewById<MapView>(Resource.Id.mapView);
            mapView.OnCreate(savedInstanceState);
            mapView.GetMapAsync(this);
            MapsInitializer.Initialize(this);

            btnRegresar.Click += delegate
            {
                RegresaraMenuHotelero();
            };

            btnGuardar.Click += async delegate
            {
                
            };

            btnEliminar.Click += async delegate
            {
                if (await borrarBlob(imagen))
                {
                    string api = "https://apihoteles.azurewebsites.net/api/hoteles/borrarhotel?idHotel=" + idHotel + "";
                    string respuesta = await TraerDatos(api);
                    Toast.MakeText(this, respuesta, ToastLength.Long).Show();
                    var intent = new Intent(this, typeof(MenuHotelero));
                    RegresaraMenuHotelero();
                    this.Finish();
                }
                else
                {
                    Toast.MakeText(this, "No se pudo eliminar", ToastLength.Long).Show();
                }

            };
        }
        public async Task<bool> borrarBlob(string nombre)
        {
            try
            {
                string archivo = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), nombre);
                var cuentaBlob = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=programacionmoviles2021;AccountKey=/MEr4ZzBjKxKAbaTKUvM0vfG14AZf2XqlLe4AQC3rE2D2q4C3oYqYeThrhNPDJCp4WaKVVxGZiDz0uCVAOgFPA==;EndpointSuffix=core.windows.net");
                var clienteBlob = cuentaBlob.CreateCloudBlobClient();
                var Carpeta = clienteBlob.GetContainerReference("munoz-victor");
                var resourceBlob = Carpeta.GetBlockBlobReference(nombre);
                await resourceBlob.DeleteIfExistsAsync();
                return true;
            }
            catch (System.Exception ex)
            {
                return false;

            }
        }
        public async Task<string> TraerDatos(string API)
        {

            var request = (HttpWebRequest)WebRequest.Create(API);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return await reader.ReadToEndAsync();

        }

        public void OnMapReady(GoogleMap googleMap)
        {

            this.googleMap = googleMap;
            var builder = CameraPosition.InvokeBuilder();
            builder.Target(new LatLng(lat, lon));
            builder.Zoom(18);
            var cameraPosition = builder.Build();
            var cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            this.googleMap.AnimateCamera(cameraUpdate);
        }

        public void RegresaraMenuHotelero()
        {
            var intent = new Intent(this, typeof(MenuHotelero));
            intent.PutExtra("correoUsuario", correo);
            intent.PutExtra("passusuario", pass);
            StartActivity(intent);
            Finish();

        }

    }
}