﻿using Android.App;
using Android.OS;
using Android.Widget;
using Plugin.CurrentActivity;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AppMovil
{
    [Activity(Label = "Reservacion")]
    public class Reservacion : Activity
    {  
        EditText txtResponsable, txtTelefono, txtHabitaciones;
        TextView txtNombreHotel;
        int idHotel;
        string Nombre;
        CalendarView calendario;
        DateTime newdatetime;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.reservacion);
            var btnRegresar = FindViewById<Button>(Resource.Id.btnRegresar);
            var btnReservar = FindViewById<Button>(Resource.Id.btmReservar);
            txtResponsable = FindViewById<EditText>(Resource.Id.txtNombreResponsable);
            txtTelefono = FindViewById<EditText>(Resource.Id.txtTelefono);
            txtHabitaciones = FindViewById<EditText>(Resource.Id.txtHabitaciones);
            txtNombreHotel = FindViewById<TextView>(Resource.Id.txtNombreHotel);
            calendario = FindViewById<CalendarView>(Resource.Id.calendarView);
            calendario.DateChange += CalendarOnDateChange;
            

            cargarDatosIntent();
            txtNombreHotel.Text = Nombre;

            btnReservar.Click += async delegate
            {
                try
                {                   
                    var Fecha = newdatetime;
                    var NombreHotel = Nombre;
                    var Responsable = txtResponsable.Text;
                    var Telefono = txtTelefono.Text;
                    var Habitaciones = txtHabitaciones.Text;           
                    var Id = idHotel;
                    var API = "https://apihoteles.azurewebsites.net/api/Hoteles/RegistrarReservacion?nombreResponsable=" + Responsable +
                    "&numpersonas=" + Habitaciones + "&fechaLlegada=" + Fecha + "&fechaSalida=" + Fecha + "&fkhotel=" + idHotel + "";
                    string respuesta = await GuardarRegistros(API);
                    Limpiar();
                    Toast.MakeText(this, respuesta, ToastLength.Long).Show();
                    
                }
                catch (System.Exception ex)
                {
                    Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                }
            };
            


            btnRegresar.Click += delegate
            {
                this.Finish();
            };

            
        }
        public void cargarDatosIntent()
        {
            idHotel = int.Parse(Intent.GetStringExtra("idHotel"));
            Nombre = Intent.GetStringExtra("nombre");

        }

        public void CalendarOnDateChange(object sender, CalendarView.DateChangeEventArgs args)
        {
             newdatetime = new DateTime(args.Year, args.Month, args.DayOfMonth);
        }

        public void Limpiar()
        {
            txtHabitaciones.Text = "";
            txtNombreHotel.Text = "";
            txtResponsable.Text = "";
            txtTelefono.Text = "";
            idHotel = 0;
            Nombre = "";
            calendario.ClearFocus();
        }

        public async Task<string> GuardarRegistros(string API)
        {
            var request = (HttpWebRequest)WebRequest.Create(API);
            byte[] data = UTF8Encoding.UTF8.GetBytes(API);
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/json; charset=utf-8";
            Stream postStream = request.GetRequestStream();
            postStream.Write(data, 0, data.Length);
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return await reader.ReadToEndAsync();
        }
    }
}