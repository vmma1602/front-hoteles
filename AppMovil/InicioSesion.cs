﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AppMovil
{
    [Activity(Label = "InicioSesion")]
    public class InicioSesion : Activity
    {

        TextView txtCrearCuenta;
        Button btnIniciarSesion;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.iniciar_sesion);
            txtCrearCuenta = FindViewById<TextView>(Resource.Id.lblRegistrarme);
            btnIniciarSesion = FindViewById<Button>(Resource.Id.btnIniciar);
            var txtCorreoLogin = FindViewById<TextView>(Resource.Id.txtDireccion);
            var txtPass = FindViewById<TextView>(Resource.Id.txtContraseña);

            txtCrearCuenta.Click += delegate
            {
                AbrirRegistro();

            };
            btnIniciarSesion.Click += async delegate
            {
                string respuesta = await GuardarRegistros("https://apihoteles.azurewebsites.net/api/hoteles/Login?correo=" + txtCorreoLogin.Text + "&pass=" + txtPass.Text + "");
                if (respuesta.ToString() == "true")
                    AbrirMenu();
                else
                {
                    Toast.MakeText(this, "Datos incorrectos", ToastLength.Short).Show();
                    txtCorreoLogin.Text = "";
                    txtPass.Text = "";
                }
                void AbrirMenu()
                {

                    Intent registro = new Intent(this, typeof(MenuHotelero));
                    registro.PutExtra("correoUsuario", txtCorreoLogin.Text);
                    registro.PutExtra("passUsuario", txtPass.Text);
                    StartActivity(registro);
                    Finish();
                };
            };
        }

        public async Task<string> GuardarRegistros(string API)
        {
            var request = (HttpWebRequest)WebRequest.Create(API);
            byte[] data = UTF8Encoding.UTF8.GetBytes(API);
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/json; charset=utf-8";
            Stream postStream = request.GetRequestStream();
            postStream.Write(data, 0, data.Length);
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return await reader.ReadToEndAsync();
        }
        public void AbrirRegistro()
        {
            Intent destino = new Intent(this, typeof(Registro));
            StartActivity(destino);
            Finish();
        }
    }
}