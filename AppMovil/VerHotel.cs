﻿using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.OS;
using Android.Widget;

namespace AppMovil
{
    [Activity(Label = "VerHotel")]
    public class VerHotel : Activity, IOnMapReadyCallback
    {
        TextView txtNombre, txtDescripcion, txtCorreo, txtDireccion, txtCosto, txtServicio1, txtServicio2, txtServicio3;
        
        int id;
        ImageView Imagen;
        GoogleMap googleMap;
        MapView map;
        string correo, imagen, nombre, descripcion, direccion, imagenfondo, servicio1, servicio2, servicio3;
        double costo, lat, lon;
        LatLng coordenadas = new LatLng(0, 0);
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.ver_hotel);
            var btnReservar = FindViewById<Button>(Resource.Id.btnReservar);
            var btnRegresar = FindViewById<Button>(Resource.Id.btnRegresar);

            id = int.Parse(Intent.GetStringExtra("idHotel"));
            nombre = Intent.GetStringExtra("nombre");
            descripcion = Intent.GetStringExtra("descripcion");
            direccion = Intent.GetStringExtra("direccion");
            correo = Intent.GetStringExtra("correoHotel");
            costo = double.Parse(Intent.GetStringExtra("precioHospedaje"));
            lat = double.Parse(Intent.GetStringExtra("lat"));
            lon = double.Parse(Intent.GetStringExtra("lon"));
            imagen = Intent.GetStringExtra("rutaFoto");
            

                txtNombre = FindViewById<TextView>(Resource.Id.txtNombreHotel);
                txtDescripcion = FindViewById<TextView>(Resource.Id.txtDescripcion);
                txtCorreo = FindViewById<TextView>(Resource.Id.txtCorreo);
                txtDireccion = FindViewById<TextView>(Resource.Id.txtDireccion);
                txtCosto = FindViewById<TextView>(Resource.Id.txtCosto);
                Imagen = FindViewById<ImageView>(Resource.Id.imagenFondo);
                RatingBar ratingBar = FindViewById<RatingBar>(Resource.Id.ratingBar);
                 ratingBar.Rating = 4;


            txtNombre.Text = nombre;
            
            txtDescripcion.Text = descripcion;
            txtDireccion.Text = direccion;
            txtCorreo.Text = correo.ToString();
            txtCosto.Text = costo.ToString();
            var rutaimagen = System.IO.Path.Combine(System.Environment.GetFolderPath
                (System.Environment.SpecialFolder.Personal), imagen);

            Bitmap bit = BitmapFactory.DecodeFile(rutaimagen);
            Imagen.SetImageBitmap(bit);

            map = FindViewById<MapView>(Resource.Id.mapViewVerHotel);
            map.OnCreate(savedInstanceState);
            map.GetMapAsync(this);
            MapsInitializer.Initialize(this);

            btnRegresar.Click += delegate
            {
                this.Finish();
               

            };

            btnReservar.Click += delegate
            {
                AbrirReservaciones();
            };
        }

        public void AbrirReservaciones()
        {
            Intent destino = new Intent(this, typeof(Reservacion));
            destino.PutExtra("idHotel", Intent.GetStringExtra("idHotel"));
            destino.PutExtra("nombre",Intent.GetStringExtra("nombre"));
            StartActivity(destino);
        }

        public void OnMapReady(GoogleMap googleMap)
        {

            this.googleMap = googleMap;
            var builder = CameraPosition.InvokeBuilder();
            builder.Target(new LatLng(lat, lon));
            builder.Zoom(17);
            var cameraPosition = builder.Build();
            var cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            this.googleMap.AnimateCamera(cameraUpdate);
        }

        
    }
}