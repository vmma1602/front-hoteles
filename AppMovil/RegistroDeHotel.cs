﻿using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.OS;
using Android.Widget;
using Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json.Linq;
using Plugin.CurrentActivity;
using Plugin.Media;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AppMovil
{
    [Activity(Label = "RegistroDeHotel")]
    public class RegistroDeHotel : Activity, IOnMapReadyCallback
    {
        Plugin.Media.Abstractions.MediaFile foto;
        string correoUsuario, passUsuario;
        ImageView imgCargarFoto;
        Stream stream;
        GoogleMap googleMap;
        LatLng coordenadas = new LatLng(0, 0);
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.registro_de_hotel);
            imgCargarFoto = FindViewById<ImageView>(Resource.Id.imagenHotel);
            var btnGuardarDatosHotel = FindViewById<Button>(Resource.Id.btnGuardarHotel);
            var txtNombreHotel = FindViewById<EditText>(Resource.Id.txtNombreHotel);
            var txtDescripcion = FindViewById<EditText>(Resource.Id.txtDescripcion);
            var txtDireccion = FindViewById<EditText>(Resource.Id.txtDireccion);
            var txtCosto = FindViewById<EditText>(Resource.Id.txtCosto);
            var txtCorreo = FindViewById<EditText>(Resource.Id.txtCorreo);
            var btnRegresar = FindViewById<Button>(Resource.Id.btnRegresar);
            var mapa = FindViewById<MapView>(Resource.Id.mapViewElegir2);



            btnRegresar.Click += delegate
            {
                this.Finish();
                Intent destino = new Intent(this, typeof(MenuHotelero));
                StartActivity(destino);
            };


            cargarDatosIntent();
            txtCorreo.Enabled = true;
            if (correoUsuario == "")
                correoUsuario = txtCorreo.Text;
            else
                txtCorreo.Text = correoUsuario;


            imgCargarFoto.Click += async delegate
            {
                foto = await SeleccionarFotos();
            };

            txtDescripcion.AfterTextChanged += async delegate
            {

                coordenadas = await SacarCoordenadas(txtDireccion.Text);
                mapa.OnCreate(savedInstanceState);
                mapa.GetMapAsync(this);
                MapsInitializer.Initialize(this);

            };



            btnGuardarDatosHotel.Click += async delegate
            {
                try
                {
                    if (foto == null)
                    {
                        Toast.MakeText(this, "Seleccione una foto", ToastLength.Long).Show();
                    }
                    else
                    {
                        coordenadas = await SacarCoordenadas(txtDireccion.Text);
                        string archivo = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), (txtNombreHotel.Text + ".jpg").ToString());
                        string api = "https://apihoteles.azurewebsites.net/api/hoteles/RegistrarHotel?nombre=" + txtNombreHotel.Text + "&correo=" + correoUsuario + "&pass=" + passUsuario + "&direccion=" + txtDireccion.Text + "&lon=" + coordenadas.Longitude + "&lat=" + coordenadas.Latitude + "&descripcion=" + txtDescripcion.Text + "&precioNoche=" + txtCosto.Text + "&foto1=" + txtNombreHotel.Text + "" + ".jpg";
                        using (var fs = new FileStream(archivo, FileMode.OpenOrCreate))
                        {

                            Bitmap bitmap = await BitmapFactory.DecodeStreamAsync(foto.GetStream());
                            bitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, fs);
                        }

                        bool blob = await guardarBlob(txtNombreHotel.Text + ".jpg", archivo);
                        if (blob)
                        {
                            string respuesta = await GuardarRegistros(api);
                            Toast.MakeText(this, respuesta, ToastLength.Long).Show();
                        }
                        else
                        {
                            Toast.MakeText(this, "Los datos no se pudieron cargar", ToastLength.Long).Show();
                        }


                        //Limpiar();
                    }

                }
                catch
                {

                }
                void Limpiar()
                {
                    txtCorreo.Text = "";
                    txtCosto.Text = "";
                    txtDescripcion.Text = "";
                    txtDireccion.Text = "";
                    txtNombreHotel.Text = "";

                };

            };
        }

        public async Task<LatLng> SacarCoordenadas(string direccion)
        {
            try
            {
                var URL = $"https://maps.googleapis.com/maps/api/geocode/json" +
                    $"?address={direccion}" +
                    $"&inputtype=textquery&fields=geometry" +
                    $"&key=AIzaSyCpUx41FejxfIsnFuWFZU1BrE7DQRZ5cCg";

                var jsonString = await TraerDatos(URL);
                var responseObject = JObject.Parse(jsonString);

                var results = responseObject["results"][0]?["geometry"]?["location"];

                coordenadas.Latitude = double.Parse(results["lat"].ToString());
                coordenadas.Longitude = double.Parse(results["lng"].ToString());

                return coordenadas;

            }
            catch (Exception ex)
            {
                Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                return coordenadas;
            }
        }


        public async Task<string> TraerDatos(string API)
        {

            var request = (HttpWebRequest)WebRequest.Create(API);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return await reader.ReadToEndAsync();

        }

        public async Task<bool> guardarBlob(string nombre, string file)
        {

            try
            {

                var cuentaBlob = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=programacionmoviles2021;AccountKey=/MEr4ZzBjKxKAbaTKUvM0vfG14AZf2XqlLe4AQC3rE2D2q4C3oYqYeThrhNPDJCp4WaKVVxGZiDz0uCVAOgFPA==;EndpointSuffix=core.windows.net");
                var clienteBlob = cuentaBlob.CreateCloudBlobClient();
                var Carpeta = clienteBlob.GetContainerReference("munoz-victor");
                var resourceBlob = Carpeta.GetBlockBlobReference(nombre);
                //var fs = new FileStream(archivo, FileMode.OpenOrCreate);
                await resourceBlob.UploadFromFileAsync(file);
                return true;
            }
            catch (Exception ex)
            {
                return false;

            }

        }
        public async Task<string> GuardarRegistros(string API)
        {
            var request = (HttpWebRequest)WebRequest.Create(API);
            byte[] data = UTF8Encoding.UTF8.GetBytes(API);
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/json; charset=utf-8";
            Stream postStream = request.GetRequestStream();
            postStream.Write(data, 0, data.Length);
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return await reader.ReadToEndAsync();
        }

        public async Task<Plugin.Media.Abstractions.MediaFile> SeleccionarFotos()
        {
            await CrossMedia.Current.Initialize();
            var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,

            });

            stream = file.GetStream();
            stream.Position = 0;
            var bit = BitmapFactory.DecodeStream(stream);

            imgCargarFoto.SetImageBitmap(bit);
            //foto.Dispose();
            return file;
        }


        public void cargarDatosIntent()
        {

            correoUsuario = Intent.GetStringExtra("correoUsuario");
            passUsuario = Intent.GetStringExtra("passUsuario");

        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public void OnMapReady(GoogleMap mapa)
        {
            this.googleMap = mapa;
            var builder = CameraPosition.InvokeBuilder();
            builder.Target(coordenadas);
            builder.Zoom(18);
            var cameraPosition = builder.Build();
            var cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            this.googleMap.AnimateCamera(cameraUpdate);
        }
    }
}