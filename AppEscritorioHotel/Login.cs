﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;


namespace AppEscritorioHotel
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            txtPass.isPassword = true;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private async void btnIngresar_Click(object sender, EventArgs e)
        {
            string respuesta = await GuardarRegistros("https://apihoteles.azurewebsites.net/api/hoteles/Login?correo=" + txtCorreo.Text + "&pass=" + txtPass.Text + "");
            if (respuesta.ToString() == "true")
            {
                HostPro host = new HostPro(txtCorreo.Text, txtPass.Text);
                host.Show();
                this.Hide();
            }
            else
                MessageBox.Show("Datos incorrectos");
        }
        public async Task<string> GuardarRegistros(string API)
        {
            var request = (HttpWebRequest)WebRequest.Create(API);
            byte[] data = UTF8Encoding.UTF8.GetBytes(API);
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/json; charset=utf-8";
            Stream postStream = request.GetRequestStream();
            postStream.Write(data, 0, data.Length);
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return await reader.ReadToEndAsync();
        }
        
      
    }
}
