﻿using System.Windows.Forms;
using System.Data;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace AppEscritorioHotel
{
    public partial class HostPro : Form
    {
        string correo, pass;
        List<Reservacion> lista = new List<Reservacion>();
        public int xClick = 0, yClick = 0;
        public HostPro()
        {
            InitializeComponent();
            
               
        }
        public HostPro(string correo, string pass)
        {
            InitializeComponent();
            this.correo = correo;
            this.pass = pass;
            CargarReservaciones(correo);
            dtgDatos.DataSource = lista;
        }

        private void HostPro_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                xClick = e.X; yClick = e.Y;
            }
            else
            {
                this.Left = this.Left + (e.X - xClick); this.Top = this.Top + (e.Y - yClick);
            }
        }

        private void btnCerrar_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                xClick = e.X; yClick = e.Y;
            }
            else
            {
                this.Left = this.Left + (e.X - xClick); this.Top = this.Top + (e.Y - yClick);
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            CargarReservaciones(correo);
            dtgDatos.DataSource = lista;
        }

        public string TraerDatos(string API)
        {

            var request = (HttpWebRequest)WebRequest.Create(API);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return reader.ReadToEnd();

        }

        public void CargarReservaciones(string correo)
        {
           
            try
            {

                string Datosjson = "";
                Datosjson = TraerDatos("https://apihoteles.azurewebsites.net/api/Hoteles/listareservacionescorreo?correo="+correo+"");
                var numeroRegistros = JArray.Parse(Datosjson);
                for (int i = 0; i < numeroRegistros.Count; i++)
                {
                    JObject data = JObject.Parse(numeroRegistros[i].ToString());
                    Reservacion r = new Reservacion();
                    r = JsonConvert.DeserializeObject<Reservacion>(data.ToString());
                    lista.Add(r);
                }           

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }

    public class Reservacion
    {
        public int Codigo { get; set; }
        public int Reservaciones { get; set; }
        public string Huesped { get; set; }
        public DateTime Llegada { get; set; }
        public DateTime Salida { get; set; }
        public string Hotel { get; set; }
    }
}
